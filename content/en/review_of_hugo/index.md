I spent some time trying out Hugo for Docs as Code, you can checkout https://gitlab.com/pankajkgh/sre-docs to try it for yourself.

# Installation:

Installing Hugo was a breeze. With just a single command, “brew install hugo,” you can quickly set it up on your machine. It’s a straightforward process that gets you up and running in no time. Additionally, Hugo’s official website provides detailed installation instructions for various operating systems, making it easy for users to follow the steps and install Hugo without any issues. I found the documentation easy to follow. Hugo is written in go and is quite easy to setup.

# Local Testing:
Testing the documentation locally is quite simple with Hugo’s built-in server. By running the `hugo server` command, you can instantly preview your changes on localhost. This feature greatly facilitates the development and debugging process. It automatically detects any modifications made to your documentation files and updates the local preview, allowing you to iterate and fine-tune your content with ease. The server also provides a live reload feature, enabling you to see the changes in real-time as you make them.

# Hosting on GitLab Pages:

While hosting on GitLab Pages is possible, a license may be required or free CPUs need to be enabled. GitLab Pages allows you to automatically deploy your documentation to GitLab Pages whenever you push changes to your repository. By following the appropriate configuration steps and setting up a GitLab CI/CD pipeline, you can ensure that your documentation is continuously deployed and accessible to your audience. However, I could not find a way to add basic authentication to gitlab pages to keep it private for us, this may be possible but I did not try a lot.

# Hosting on AWS:

It is possible to deploy hugo on AWS, Hugo integrates with AWS Amplify. This allows you to deploy your documentation easily on the AWS infrastructure. By connecting your Hugo repository to an AWS Amplify app, you can automate the deployment process and take advantage of AWS’s scalable and reliable hosting capabilities. AWS Amplify supports various features, such as automatic builds, custom domains, and CDN caching, making it an excellent choice for hosting Hugo-based documentation. The official Hugo documentation provides detailed instructions on how to set up and configure the deployment to AWS Amplify.

# Themes:

Hugo offers a wide selection of themes, including dedicated themes for documentation. These themes are designed to provide a clean and professional look to the documentation site. The Hugo Themes website showcases numerous themes specifically tailored for documentation purposes. I tried a theme didicated to documentaiton and found a notable feature that adds a “edit on GitLab” link on every generated page. This link simplifies the editing process by directly linking to the corresponding page on GitLab, allowing users to make changes easily.

# Markdown Support:

Hugo provides excellent support for Markdown. With Hugo, you can create Markdown files to compose your documentation content. Hugo’s Markdown rendering is accurate and consistent. You can utilize various Markdown features such as headings, lists, code blocks, tables, and more to structure and enhance your documentation. Additionally, Hugo supports Markdown extensions like footnotes and tables of contents, enabling you to include advanced features in your documentation.

# AsciiDoc Support:

However, Hugo’s support is not as robust. While AsciiDoc files can be processed by Hugo, there are limitations to consider. One notable limitation is the handling of HTML comments within AsciiDoc files. Any AsciiDoc file starting with an HTML comment is considered HTML by Hugo and, as a result, ignored during the build process. This can be problematic for us as our AsciiDoc files contain important information or instructions within HTML comments. Therefore, we will need to review our AsciiDoc files and remove or restructure any content relying on HTML comments to ensure proper rendering with Hugo.

# Moving Existing Docs on Confluence:

Transferring existing documentation from Confluence to Hugo is not a straightforward task. Unfortunately, Confluence does not provide optimal options for exporting documentation in a format that can be easily imported into Hugo. As a result, we may need to redo our work when migrating from Confluence to Hugo. This process involves manually copying the content from Confluence and reformatting it in Markdown or AsciiDoc, depending on our chosen format. It can be time-consuming and may require additional effort.

# Search:
One area where Hugo falls short is in its built-in search functionality. The default search feature provided by Hugo is limited and may not meet all requirements for robust search capabilities. However, there are a few third-party paid options available that offer advanced search features for Hugo-based documentation. These solutions integrate with Hugo and provide features such as full-text search, filtering, faceted search, and customizable search result layouts. Alternatively, you can implement client-based search options using JavaScript libraries like Lunr-Search. These options require additional setup and configuration but client based search is not very good and requires cliend to download the index before any searches can be made. For a large site this may not work very well.