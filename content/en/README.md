# Developer Docs

In this repository you can find everything you need in regards to technical documentation, including onboarding, infrastructure, coding standards, etc. The markdowns in this repository are also rendered as Confluence pages, under `Developer Docs` in [ODC](https://greenhouse.atlassian.net/wiki/spaces/L/pages/1512308739/Developer+Docs) Confluence space. If you are interested in adding documents to this repository and have them rendered as Confluence pages, please read the next section.

# Rendering Markdowns to Confluence

In order to have your Markdown files rendered as Confluence pages, you just need to have a few things in place:
- Make sure that you have the necessary metadata at the very top of your document (you can see the example in this file or in any other Markdown that is currently being rendered). Your metadata must include at least the `<!-- Space: xxx -->` and the `<!-- Title: xxx -->` headers. If it doesn't contain any `<!-- Parent: xxx -->` header, then a page with the specified `Title` will be rendered at the root of the space.
- Add a path to your Markdown (relative to the root of the repository) to the file `.confluence.yaml`. You can find this file at the root of the repository.
- If your file has any attachment, then the metadata at the beginning of your file must contain a `<!-- Attachment: path/to/attachment -->` header. Note that the path to the attachment must be relative to the root of the repository, and not relative to the markdown itself. The same goes for the `[alt]` or `[alt text]` directives: the path to the resource must be relative to the root of the repository.

## Limitations and Caveats

- Confluence doesn't allow two pages in the same space to have the same title, even if they live under a different hierarchy. As such, it's important that you make sure that the title you choose for the `<!-- Title: xxx -->` header doesn't exist in the namespace already. When the page is re-rendered this won't matter match, as its contents will be re-written, but if you're rendering a page for the first time and there is already a page with the same title under a different parent hierarchy, then this is where your page will end up.

# Table of Contents

- [Architecture](architecture/index.md)
 - [Architecture Decision Records](architecture/adr/index.md)
- [How-Tos](howtos/index.md)
- [Onboarding](onboarding/index.md)
 - [Clojure](onboarding/clojure.md)
 - [ClojureScript](onboarding/clojurescript/index.md)
 - [JavaScript](onboarding/javascript/index.md)
 - [Python](onboarding/python/index.md)
- [Proposals](proposals/index.md)
- [Presentations](presentation/index.md)
- [Style Guide](styleguide/index.md)
- [Team](team/index.md)

# Scedules

[Scedule of error shifts and release managers](https://docs.google.com/spreadsheets/d/1V-2SUnDOzETYqsoQZ7u9onRmWu8cgso5ghReCvD1JiM/edit#gid=34690403)
