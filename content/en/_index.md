---
title: "Developer Documentation"
description: "In this repository you can find everything you need in regards to technical documentation, including on-boarding, infrastructure, coding standards, etc."
menu:
  main:
    weight: 1
cascade:
  featured_image: '/images/choreograph.png'
---
# Table of Contents
- [Architecture](architecture/index.md)
- [Architecture Decision Records](architecture/adr/index.md)
- [How-Tos](howtos/index.md)
- [Onboarding](onboarding/index.md)
- [Clojure](onboarding/clojure.md)
- [ClojureScript](onboarding/clojurescript/index.md)
- [JavaScript](onboarding/javascript/index.md)
- [Python](onboarding/python/index.md)
- [Proposals](proposals/index.md)
- [Presentations](presentation/index.md)
- [Style Guide](styleguide/index.md)
- [Site Reliability](sre/index.md)
- [Team](team/index.md)

# Scedules

[Scedule of error shifts and release managers](https://docs.google.com/spreadsheets/d/1V-2SUnDOzETYqsoQZ7u9onRmWu8cgso5ghReCvD1JiM/edit#gid=34690403)
