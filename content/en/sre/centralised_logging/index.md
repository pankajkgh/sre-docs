﻿---
title: "Centralised Logs"
description: "How to access Open search"
featured_image: '/images/Victor_Hugo-Hunchback.jpg'
menu:
  main:
    weight: 1
---
# **How to access Open search**
To look for debugging logs for the any-microservice service on <https://logsearch.lemonpi.io/_dashboards/,> you can follow these steps:

1. Access the URL: Open a web browser and go to [https://logsearch.lemonpi.io/_dashboards/.](https://logsearch.lemonpi.io/_dashboards/)
   1. You will be redirected to a login screen.
   1. Log in: If you have credentials for accessing the log search dashboard, log in using your @choreograph.com email and password. 
   1. If you never logged in before. You will be asked to type a new password.
   1. If you forgot your password #cc-technical-excellence can reset your password to a temp password
   1. Login and choose a two factor authentication method 
1. Can’t log in. Contact the #cc-technical-excellence who can provide access to the log search dashboard.
1. Select your tenant, you want to use **Global** to access existing dashboards made by others and switch to **Private** when you’re building dashboards just for yourself.
1. Navigate to the dashboards: Once logged in, navigate to the dashboards section of the log search platform. 
1. <https://logsearch.lemonpi.io/_dashboards/app/visualize#/edit/41773c50-0aac-11ee-9294-cf8498a2e423>
## **List of existing dashboards** 
Some micro-services already have some structured data and we can have dashboards for them. We will add more here as and when more dashboards become available. 

|[Lemonpi services logs](https://logsearch.lemonpi.io/_dashboards/app/dashboards#/view/33baf390-ef22-11ed-bbc4-cd68a166e67f)|
| :- |
|[\[lemonpi\] logs-advertiser-api](https://logsearch.lemonpi.io/_dashboards/app/discover#/view/b7678e60-edc9-11ed-bbc4-cd68a166e67f)|
|[\[lemonpi\] logs-interaction-handler](https://logsearch.lemonpi.io/_dashboards/app/discover#/view/a2732d20-edc9-11ed-9294-cf8498a2e423)|
|[\[lemonpi\] logs-manage-ui](https://logsearch.lemonpi.io/_dashboards/app/discover#/view/c8e275c0-edc8-11ed-8feb-a3267c358bb1)|
|[\[lemonpi\] logs-content-variant/variant-query-results](https://logsearch.lemonpi.io/_dashboards/app/discover#/view/df7a3310-ee52-11ed-9294-cf8498a2e423)|
|[\[lemonpi\] logs-geo-location-v2](https://logsearch.lemonpi.io/_dashboards/app/discover#/view/69159cf0-ee4d-11ed-bbc4-cd68a166e67f)|
|[\[lemonpi\] logs-auth](https://logsearch.lemonpi.io/_dashboards/app/discover#/view/8eac4f60-edc9-11ed-9294-cf8498a2e423)|
|[\[lemonpi\] logs-weather](https://logsearch.lemonpi.io/_dashboards/app/discover#/view/1c00fb00-ee64-11ed-8feb-a3267c358bb1)|
|[\[lemonpi\] logs-content-api](https://logsearch.lemonpi.io/_dashboards/app/discover#/view/cec28030-ee49-11ed-a865-0584a5646b89)|
|[\[lemonpi\] logs-content-variant](https://logsearch.lemonpi.io/_dashboards/app/discover#/view/43ff9400-ee54-11ed-a865-0584a5646b89)|
|[\[lemonpi\] logs-api-gateway](https://logsearch.lemonpi.io/_dashboards/app/discover#/view/17a09e30-ee50-11ed-8feb-a3267c358bb1)|
|[\[lemonpi\] logs-helm-controller](https://logsearch.lemonpi.io/_dashboards/app/discover#/view/86877ad0-ee55-11ed-bbc4-cd68a166e67f)|
|[\[lemonpi\] logs-product-selector-api](https://logsearch.lemonpi.io/_dashboards/app/discover#/view/091f77a0-edc9-11ed-8feb-a3267c358bb1)|

Follow the trainings under this section for more information.
