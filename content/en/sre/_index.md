---
title: "Site Reliability Engineering"
description: "SREs bring in engineering practices to keep services up. This page serves as a gateway to SRE practices followed by the Core SRE team."
menu:
  main:
    weight: 1
---
Site Reliability Engineers (SREs) sits at the intersection of software engineering and systems engineering. While there are potentially infinite permutations and combinations of how infrastructure and software components can be put together to achieve an objective, focusing on foundational skills allows SREs to work with complex systems and software, regardless of whether these systems are proprietary, 3rd party, open systems, run on cloud/on-prem infrastructure, etc.

SREs bring in engineering practices to keep services up. This page serves as a gateway to SRE practices followed by the Core SRE team.

We validate business requirements, convert them to SLAs for each of the components that constitute the distributed system, monitor and measure adherence to SLAs, re-architect or scale out to mitigate or avoid SLA breaches, add these learnings as feedback to new systems or projects and thereby reduce operational toil.

# Eliminating Toil

Toil is manual work that is repetitive and automate-able. Toil is interrupt-driven and reactive rather than strategy driven. Handling pager or creating servers by clicking around or typing commands is toil. Toil is work with no enduring value. Toil creates low morale, tribal knowledge, slows progress and promotes attrition. We measure toil and strive to keep Toil under 50% of every engineer's time.

# Resiliency

Resiliency is the ability to maintain acceptable service levels in the face of adversity. Site Reliability Engineering seeks to balance the risk of an outage with the goals of innovation and efficient operations, so that users’ overall happiness with features, service, and performance is optimised. We tune our service resiliency based on user expectations.

# Observability

Observability is the measure of how well internal states of a system can be understood from outside the service. What can be understood about a service without reading code and logging into servers and databases. Good observability means that you have the ability to  Understand the inner workings of your application and understand any system state your application many have gotten itself into no matter how extreme or unusual

# Incident Response

Failures are a given. Everything will eventually fail over time. SREs make sure that services perform in the face of failure as well as know how to deal with failure scenarios.

When you are dealing with the outages related to complex systems, Rational, focused, and deliberate cognitive functions save the day. SREs prepare for Incidents by performing drills and chaos tests and validating assumptions and corrective actions.

SREs keep the on-call workload balanced and sustainable across two axes:
- Balance in quantity
- Balance in quality

SREs rely on several resources that make the experience of being on-call less daunting than it may seem.

- Clear escalation path 
- Well-defined incident-management procedures 
- A blameless post mortem culture
